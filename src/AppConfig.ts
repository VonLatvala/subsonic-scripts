export type SubsonicConfig = {
    username: string,
    password: string,
    url: string,
}

export type AppConfig = {
    subsonic: SubsonicConfig,
}
