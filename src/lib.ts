import axios from 'axios';
import md5 from 'md5';
import * as crypto from 'crypto';

import { SubsonicAuthData, SubsonicBookmark } from './SubsonicTypes';

const subsonicRequest = async (subsonicUrl: string, path: string, authData: SubsonicAuthData, requestData?: Record<string, unknown>) => {
    const salt = crypto.randomBytes(16).toString('base64');
    const token = md5(authData.password + salt);

    const res = await axios.get(`${subsonicUrl}/rest/${path}`, {
        params: Object.assign({}, {
            f: 'json',
            v: '1.16.1',
            c: 'subsonic-scripts',
            t: token,
            s: salt,
            u: authData.username,
            ...requestData
        })
    });

    if(res.status == 200) {
        return res.data['subsonic-response'];
    } else {
        throw new Error(`Problem speaking to Subsonic: ${res.statusText}`);
    }
};

export const getBookmarks = async (subsonicUrl: string, authData: SubsonicAuthData): Promise<SubsonicBookmark[]> => {
    const res = await subsonicRequest(subsonicUrl, 'getBookmarks', authData);

    return res.bookmarks.bookmark;
};

export const removeBookmarks = async (subsonicUrl: string, authData: SubsonicAuthData, bookmarkIds: string[]): Promise<string[]> => {

    const promises: Promise<Record<string, unknown>>[] = [];
    bookmarkIds.forEach((bookmarkId) => {
        promises.push(subsonicRequest(subsonicUrl, 'deleteBookmark', authData, { id: bookmarkId }));
    });

    await Promise.all(promises);

    return bookmarkIds;
};
