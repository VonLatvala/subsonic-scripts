import {AppConfig} from './AppConfig';
import {getBookmarks, removeBookmarks} from './lib';

export default async (config: AppConfig): Promise<void> => {

    const subsonicAuthData = { username: config.subsonic.username, password: config.subsonic.password };

    const bookmarks = await getBookmarks(config.subsonic.url, subsonicAuthData);

    bookmarks.forEach((bookmark) => {
        console.log(`Bookmark with ID ${bookmark.entry.id}, title ${bookmark.entry.title}`);
    });

    const bookmarkIds = bookmarks.map((bookmark) => bookmark.entry.id);

    const deletedIds = await removeBookmarks(config.subsonic.url, subsonicAuthData, bookmarkIds);

    console.log(`Deleted following bookmarks from subsonic: ${deletedIds.join(',')}`);
};
