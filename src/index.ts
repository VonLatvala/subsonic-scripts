import * as dotenv from 'dotenv';

import app from './app';

dotenv.config();

if(typeof process.env.SUBSONIC_URL == 'undefined') {
    throw new Error('Missing dotenv variable SUBSONIC_URL');
}
if(typeof process.env.SUBSONIC_USERNAME == 'undefined') {
    throw new Error('Missing dotenv variable SUBSONIC_USERNAME');
}
if(typeof process.env.SUBSONIC_PASSWORD == 'undefined') {
    throw new Error('Missing dotenv variable SUBSONIC_PASSWORD');
}

app({ subsonic: { url: process.env.SUBSONIC_URL, username: process.env.SUBSONIC_USERNAME, password: process.env.SUBSONIC_PASSWORD } });
