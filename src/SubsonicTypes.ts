export type SubsonicBookmarkEntry = {
    id: string;
    parent: string;
    isDir: boolean;
    title: string;
    album: string;
    size: number;
    contentType: string;
    suffix: string;
    duration: number;
    bitRate: number;
    path: number;
    isVideo: boolean;
    playCount: number;
    created: string;
    type: string;
    bookmarkPosition: number;
    originalWidth: number;
    originalHeight: number;
}

export type SubsonicBookmark = {
    position: number;
    username: string;
    comment: string;
    created: string;
    changed: string;
    entry: SubsonicBookmarkEntry;
}

export type SubsonicAuthData = {
    username: string,
    password: string,
}
