# Subsonic Scripts

## What's this

Some automation using subsonic API, features subsonic itself won't expose in the UI.

## How do I get started

1. Copy file `.env.example` to `.env`, and change details in there, should be pretty self-explainatory.
2. Run `yarn install`
3. Run `yarn dev`

## Current situation

Currently the software lists and clears users bookmarks, which is pretty useful as atleast I enjoy to hear full songs instead of just the ends of them.

## Author

Axel Latvala
